import kabaret.app.resources as resources
resources.add_folder('icons.flow', __file__)

# Some Icons are:
# Icons made by Dave Gandy from www.flaticon.com, licensed by CC 3.0 BY (http://creativecommons.org/licenses/by/3.0/)
# <div>Icons made by <a href="https://www.flaticon.com/authors/dave-gandy" title="Dave Gandy">Dave Gandy</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

