Create My Studio
================


Goal
----

The Kabaret framework covers many aspects of a TD needs. The most basic one might be to present a GUI to the Artists with some tools to execute.

We are going to build such a GUI with kabaret.

Prerequisites
-------------

For this tutorial we assume you have installed kabaret in either options described `here <../install.html>`_, and have a local redis-server as described in the previous tutorial `prerequisites <./demo_show_case.html#prerequisites>`_.

Preparation
-----------

Choose a folder where you want to put your code. This location will be referred to as *<BASEDIR>*.

We will need to import python code from there, so you should have it in your PYTHONPATH (or use any other trick you flavor...).

Let's do it !
-------------

We are going to create a python package containing all the code using kabaret.
There is a convention to name such package as *<xxx>_studio* since they tend to contain all the proprietary code you need to run a studio. So let's name ours *'my_studio'*.

Create the *<BASEDIR>/my_studio* folder and add a *__init__.py* file inside it.

Now we are going to create a module that builds and shows our GUI. Let's have it as *my_studio.gui*.

Create the *<BASEDIR>/my_studio/gui.py* file, and open it in your favorite text editor.

Kabaret applications are managed as *'sessions'*. All sessions in the local network communicate with each other so that you can build a truly collaborative application for your users. But you may need to handle more than one studio in a single network so in order to restrict those communications, sessions are organized in *clusters*.

Another purpose of the session is to provide an API to kabaret features and kabaret extensions features. This API is composed by collections of *commands*. A session contains a configurable list of *Actors*, and each actor defines a single collection of commands.

There are a couple of session types available in the framework. One is a *Standalone GUI Session*, and we are going to use it.

In the *gui.py* file, import the kabaret.app.ui.gui module and subclass the *KabaretStandaloneGUISession* it contains:

.. code-block:: python
    :linenos:

    from kabaret.app.ui import gui


    class MyStudioGUISession(gui.KabaretStandaloneGUISession):

        pass

Now let's have our *gui* module act as a main by adding the classic *__name__* test and create our session. Add those lines at the end of *gui.py*:


.. code-block:: python
    :linenos:

    if __name__ == '__main__':
        session = MyStudioGUISession(session_name="MyStudio")
        session.cmds.Cluster.connect(
            host='localhost', 
            port='6379',
            cluster_name='TUTORIALS',
            db_index='1'
        )
        session.start()
        session.close()

Here we create our session, giving it a name which will help identify it in the cluster and in logs. We use the *'connect'* command of the *'Cluster'* Actor to configure the communication with other sessions. We start the session and close it after the last window of the GUI get destroyed.

You can now launch you very own application using python's *-m* flag:

.. code-block:: shell

    python -m my_studio.gui

Windows users may want to create a *.bat* file containing something like:

.. code-block:: cmd

    set PYTHONPATH=%PYTHONPATH%;<BASEDIR>
    C:\python27\python.exe -m my_studio.gui
    pause

You should see the classic default Kabaret window, with a project explorer view:

.. figure:: demo_show_case/empty_kabaret_standalone.png
   :scale: 50 %
   :alt: Empty Kabaret Standalone

   Your very own GUI \\o/

And in the shell you should be able to see: 

.. code-block:: shell

    kabaret - INFO: Registering 'Cluster' Actor from kabaret.app.actors.cluster
    kabaret - INFO: Registering 'Flow' Actor from kabaret.app.actors.flow
    kabaret - INFO: Connecting to localhost port:'6379', index:'1'
    kabaret - INFO: Connected to Cluster 'TUTORIALS'
    kabaret - INFO: Configuring Project Registry
    kabaret - INFO: Subcribing to flow_touched messages.
    kabaret - INFO: [Broadcast Message] u'Cluster joined by Dee:MyStudio-8872@Dee-PC'

This may not seem like much but less than 10 python lines you have built a highly configurable and extensible application that can communicate with everyone in the local network. If you click on the **'*'** button on the top right corner of the default view, you will see that this application has a classic multi-view interface where you can drag'n'drop views to move and/or stack them.

Optional fun
------------

Before adding actual useful things into this GUI, let's see how we can customize it, just for fun :)

In *gui.py*, add those lines just before the *__name__* test:

.. code-block:: python
    :linenos:

    from kabaret.app.ui.gui.styles import Style


    class NoStyle(Style):

        def apply(self, widget):

            pass


    NoStyle('NoStyle')


We've created and applied a custom style to the gui. This style does nothing in its *apply()* method so if you launch your GUI you will now have something looking like the default for your current Operating System theme.

Now let's do something more interesting by subclassing the default style and rebranding it to a bluish identity. Add those lines just before the *__name__* test:

.. code-block:: python
    :linenos:

    from qtpy import QtGui
    from kabaret.app.ui.gui.styles import dark


    class MyStyle(dark.DarkStyle):

        def apply(self, widget):
            super(MyStyle, self).apply(widget)

            palette = widget.palette()
            palette.setColor(palette.Window, QtGui.QColor('#556'))
            palette.setColor(palette.Base, QtGui.QColor('#335'))
            palette.setColor(palette.Highlight, QtGui.QColor('#002'))
            palette.setColor(palette.HighlightedText, QtGui.QColor('#88D'))
            widget.setPalette(palette)


    MyStyle()


We've created a new style based on the default one and we have overridden a few color settings to have a nice (?!) blue ambience.

There's way more you can do with the framework like using stylesheets, replacing or adding icons, etc. But the default theme and icons have been carefully crafted and selected for a nice CG Artist experience.


Conclusion
----------

The philosophy of Kabaret is to provide high-level features but also to reduce the boilerplate to the strict minimum without closing the door to customization and personalization. 

Now that you have the environment set up (1 folder and 2 files !) you can build a collaborative application with a classic multi-view GUI.

In the next chapter we will see how convenient and efficient this can be for your workflow/pipeline users.


