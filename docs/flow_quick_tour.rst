================
Project Modeling 
================

.. warning:: Documentation in Progress...


The Flow idea
=============


Parenting Objects
=================

Child Relation
--------------

Parent Relation
---------------


Adding Parameters
=================
	
Param / Values
--------------

Typed Values
------------

Custom Values
-------------

Computed Values
---------------


Executing Actions
=================

Affect your flow
----------------

Run applications
----------------

Long Running Tasks
------------------


Connecting Objects
==================

Connection Relation
-------------------

Connection vs Child
-------------------


Maps
====

Why Maps
--------

Map
---

Dynamic Map
-----------

